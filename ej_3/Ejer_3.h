#include <string>
#include "Postre.h"
#include "Nodo.h"
#include "Lista.h"
#include "Nodo_postre.h"
#include "Lista_pos.h"
using namespace std;



class Ejercicio {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Ejercicio() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};
