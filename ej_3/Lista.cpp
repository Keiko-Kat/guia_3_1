#include <iostream>
#include <string>
using namespace std;
#include "Nodo_postre.h"
#include "Postre.h"
#include "Lista_pos.h"
#include "Nodo.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear_orden (string nombre) {
    Nodo *tmp;
    Nodo *temp = this->raiz;
	Nodo *repl;

    /* crea un nodo . */
    tmp = new Nodo;
    /* asigna la instancia de Nombre. */
    tmp->postre->set_nombre(nombre);
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, se compara con el resto de los nombres de la lista. */
    }else {		
		while (temp != NULL) {
			/*si es menor al primero se inserta en su lugar*/
			if(tmp->postre->get_nombre() < temp->postre->get_nombre()){
				this->raiz = tmp;
				this->raiz->sig = temp;
				temp = NULL; break;
			}else{
				if(tmp->postre->get_nombre() >= temp->postre->get_nombre()){					
					if(temp->sig != NULL){
						repl = temp->sig;
						if(tmp->postre->get_nombre() <= temp->sig->postre->get_nombre()){
							temp->sig = tmp;
							tmp->sig = repl;
							temp = NULL; break;
						}
					}else{
						this->ultimo->sig = tmp;
						this->ultimo = tmp;
						temp = NULL; break;
					}
					
					temp = temp->sig;
				}
			}
		}
	}
}



void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;    
	cout<<"Nombres: "<<endl;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    while (tmp != NULL) {
		Lista_pos *tems = tmp->postre->get_lista_pos();
        cout <<"--"<< tmp->postre->get_nombre() <<" "<<endl;
        tems->imprimir_pos();
        tmp = tmp->sig;
    }
    cout<< endl;
}
