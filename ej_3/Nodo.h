#include <string>
#include <iostream>
#include "Nodo_postre.h"
#include "Lista_pos.h"
#include "Postre.h"
using namespace std;

#ifndef NODO_H
#define NODO_H

/* define la estructura del nodo. */
typedef struct _Nodo {
    Postre *postre = new Postre;
    struct _Nodo *sig;
} Nodo;

#endif
