#include "Nodo.h"
#include "Lista.h"
#include "Ejer_3.h"
#include "Postre.h"
#include "Nodo_postre.h"
#include "Lista_pos.h"
#include <string>
#include <iostream>
#include <algorithm>
using namespace std;


int main (void) {
    Ejercicio e = Ejercicio();
    Lista *lista = e.get_lista();
    string in;
    int tmp = 0, ing = 0;
    
    while(tmp != 4){
		bool is_in = false, in_is = false;
		Lista_pos *lista_pos = NULL;
		Nodo *temp = NULL;
		Nodo *nod = NULL;
		Nodo_pos *node = NULL;
		Nodo_pos *tremp = NULL;
		
		cout<<"Agregar postre            [1]"<<endl;
		cout<<"Remover postre            [2]"<<endl;	
		cout<<"Editar postre             [3]"<<endl;
		cout<<"Salir                     [4]"<<endl;
		cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
		cout<<"Opción:  "<<endl;
		cout<<"~~~~~>";
		getline(cin, in);
		tmp = stoi(in);
		if(tmp == 3){
			cout<<"    Agregar ingrediente       [5]"<<endl;
			cout<<"    Remover ingrediente       [6]"<<endl;
			cout<<"    Salir                     [4]"<<endl;
			cout<<"    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
			cout<<"    Opción:  "<<endl;
			cout<<"    ~~~~~>";
			getline(cin, in);
			tmp = stoi(in);
		}
		
		switch (tmp){
			case 1: cout<<"Ingrese el nombre de su postre:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					transform(in.begin(), in.end(),in.begin(), ::toupper);
					lista->crear_orden(in);    
					lista->imprimir();
					break;
			case 2: cout<<"Ingrese el nombre del postre a eliminar:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);				
					transform(in.begin(), in.end(),in.begin(), ::toupper);	
					nod = lista->raiz;
					if(nod->postre->get_nombre() == in){
						if(nod->sig != NULL){
							lista->raiz = lista->raiz->sig;
							is_in = true;
						}else{
							lista->raiz = NULL;
							lista->ultimo = NULL;
							is_in = true;						
						}
					}else{
						for(temp = lista->raiz; nod != NULL; temp = temp->sig){
							if(nod != lista->ultimo){
								nod = nod->sig;		
							}
							if(temp->postre->get_nombre() == in){
								if(temp == lista->ultimo){
									Nodo *prev = lista->raiz;
									while(prev->sig->postre->get_nombre() < temp->postre->get_nombre()){
										prev = prev->sig;
									}
									prev->sig = NULL;
									lista->ultimo = prev;
									temp = NULL;
								}else{	
									temp->postre = nod->postre;
									temp->sig = nod->sig;
									nod = NULL;						
								}
								lista->imprimir();
								is_in = true;
								
								break;
							}else{
								if(temp == lista->ultimo){
									nod = NULL;
								}
							}
						}
					}						
					if(is_in == false){
						cout<<"el postre que busca no se encuentra"<<endl;
					}
					
					
					break;
					
			case 4: break;
			
			case 5: lista->imprimir();
					cout<<"Ingrese el nombre del postre a editar:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);				
					transform(in.begin(), in.end(),in.begin(), ::toupper);
					
					for(temp = lista->raiz; temp != NULL; temp = temp->sig){					
						if(temp->postre->get_nombre() == in){
							while(ing == 0){
								cout<<"Ingrese ingrediente:"<<endl;
								cout<<"   ~~~> ";
								getline(cin, in);				
								transform(in.begin(), in.end(),in.begin(), ::toupper);
								temp->postre->get_lista_pos()->crear_orden(in);
								lista->imprimir();
								
								cout<<"presione 0 si desea continuar "<<endl;
								cout<<"oresione 2 si desea regresar al menu"<<endl;
								cout<<"    Opción:  "<<endl;
								cout<<"    ~~~~~>";
								getline(cin, in);
								ing = stoi(in);								
							}
							is_in = true;
							break;
						}
					}						
					if(is_in == false){
						cout<<"el postre que busca no se encuentra"<<endl;
					}
					break;
			
			case 6: lista->imprimir();
					cout<<"Ingrese el nombre del postre a editar:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);				
					transform(in.begin(), in.end(),in.begin(), ::toupper);

					nod = lista->raiz;
					for(temp = lista->raiz; nod != NULL; temp = temp->sig){
						if(temp->postre->get_nombre() == in){
							nod = NULL;
							is_in = true;								
							cout<<"Ingrese ingrediente:"<<endl;
							cout<<"   ~~~> ";
							getline(cin, in);				
							transform(in.begin(), in.end(),in.begin(), ::toupper);
							lista_pos = temp->postre->get_lista_pos();
							node = lista_pos->raiz;
							if(node->nombre == in){
								if(node != NULL){
									lista_pos->raiz = lista_pos->raiz->sig;
									in_is = true;
								}else{
									lista_pos->raiz = NULL;
									lista_pos->ultimo = NULL;
									in_is = true;						
								}
							}else{
								for(tremp = lista_pos->raiz; node != NULL; tremp = tremp->sig){
								
									if(node != lista_pos->ultimo){
										node = node->sig;		
									}
									if(tremp->nombre == in){
										if(tremp == lista_pos->ultimo){
											Nodo_pos *prevs = lista_pos->raiz;
											while(prevs->sig->nombre < tremp->nombre){
												prevs = prevs->sig;
											}
											prevs->sig = NULL;
											lista_pos->ultimo = prevs;
											tremp = NULL;
										}else{
											tremp->nombre = node->nombre;
											tremp->sig = node->sig;
											node = NULL;				
										}
										lista->imprimir();
										in_is = true;
										break;
										
									}else{
										if(tremp == lista_pos->ultimo){
											node = NULL;
										}
									}
								}
							}
						
						}else{
							if(temp == lista->ultimo){
								nod = NULL;
							}
						}
						
					}				
							
					if(is_in == false){
						cout<<"el postre que busca no se encuentra"<<endl;
						break;
					}						
					if(in_is == false){
						cout<<"el ingrediente que busca no se encuentra"<<endl;
					}
					lista->imprimir();
					break;
					
			default: cout<<"Ingreso invalido, intente nuevamente"<<endl;
		}
	}
	
	
	
    return 0;
}
