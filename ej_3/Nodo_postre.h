#include <string>
#include <iostream>
using namespace std;
#ifndef NODO_P_H
#define NODO_P_H

/* define la estructura del nodo. */
typedef struct _Nodo_pos {
    string nombre;
    struct _Nodo_pos *sig;
} Nodo_pos;

#endif
