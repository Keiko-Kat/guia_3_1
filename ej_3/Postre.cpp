#include <string>
#include <iostream>
#include <algorithm>
#include "Postre.h"
#include "Nodo_postre.h"
#include "Lista_pos.h"
using namespace std;

//constructor
Postre::Postre(){
	this->lista_pos = new Lista_pos();
	this->nombre = "\0";
}
void Postre::set_nombre(string nombre){
	this->nombre = nombre;
}
//getters
Lista_pos *Postre::get_lista_pos(){
	return this->lista_pos;
}
string Postre::get_nombre(){
	return this->nombre;
}

