#include <iostream>
#include <string>
using namespace std;

#include "Nodo_postre.h"
#include "Lista_pos.h"

Lista_pos::Lista_pos() {}

void Lista_pos::crear_orden (string nombre) {
    Nodo_pos *tmp;

    /* crea un nodo_pos . */
    tmp = new Nodo_pos;
    /* asigna la instancia de Nombre. */
    tmp->nombre = nombre;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;

    /* si el es primer nodo_pos de la lista, lo deja como raíz y como último nodo_pos. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo_pos al nuevo y deja el nuevo como el último de la lista. */
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}


void Lista_pos::imprimir_pos() {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo_pos *tmp = this->raiz;
	cout<<"Ingredientes: "<<endl;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    while (tmp != NULL) {
        cout <<"-"<< tmp->nombre <<" ";
        tmp = tmp->sig;
    }
    cout<<endl;
    cout<<"------"<< endl;
}
