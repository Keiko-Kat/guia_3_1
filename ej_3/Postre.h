#include <string>
#include <iostream>
#include <algorithm>
#include "Nodo_postre.h"
#include "Lista_pos.h"
using namespace std;

#ifndef POSTRE_H
#define POSTRE_H
class Postre{
	private:
		string nombre;
		Lista_pos *lista_pos = NULL;
	public:
	//contructor
		Postre();
		void set_nombre(string nombre);
	//getters
		Lista_pos *get_lista_pos();
		string get_nombre();
};
#endif
