#include <iostream>
#include "Nodo_postre.h"
#include <string>

using namespace std;

#ifndef LISTA_P_H
#define LISTA_P_H

class Lista_pos {
    public:
        Nodo_pos *raiz = NULL;
        Nodo_pos *ultimo = NULL;

        /* constructor*/
        Lista_pos();
        /* crea un nuevo nodo, recibe un ingrediente. */
        void crear_orden(string nombre);
        /* imprime la lista. */
        void imprimir_pos();
};
#endif
