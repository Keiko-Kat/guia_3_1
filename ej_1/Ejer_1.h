#include "Nodo.h"
#include "Lista.h"
using namespace std;



class Ejercicio {
    private:
        Lista *lista = NULL;
        Lista *lista_pos = NULL;
        Lista *lista_neg = NULL;

    public:
        /* constructor */
        Ejercicio() {
            this->lista = new Lista();
            this->lista_neg = new Lista();
            this->lista_pos = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
        Lista *get_lista_pos() {
            return this->lista_pos;
        }
        Lista *get_lista_neg() {
            return this->lista_neg;
        }
};
