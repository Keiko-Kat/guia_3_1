#include "Nodo.h"
#include "Lista.h"
#include "Ejer_1.h"
#include <string>
#include <iostream>
using namespace std;


int main (void) {
    Ejercicio e = Ejercicio();
    Lista *lista = e.get_lista();
    Lista *lista_neg = e.get_lista_neg();
    Lista *lista_pos = e.get_lista_pos();
    string in;
    int num, temp = 0;
    
    while(temp == 0){
		cout<<"Ingrese un numero entero:"<<endl;
		cout<<"   ~~~> ";
		getline(cin, in);
		num = stoi(in);
    
		lista->crear(num);    
		lista->imprimir();
				
		
		cout<<"Para ingresar otro numero ingrese 0"<<endl;
		cout<<"Para salir ingrese cualquier numero"<<endl;
		cout<<"   ~~~> ";
		
		getline(cin, in);
		temp = stoi(in);
		
	}
	
	Nodo *orden = NULL;
	for(orden = lista->raiz; orden != lista->ultimo; orden = orden->sig){
		if(orden->numero >= 0){
			lista_pos->crear_orden(orden->numero);
		}else{
			lista_neg->crear_orden(orden->numero);
		}
	}
	if(lista->ultimo->numero >= 0){
		lista_pos->crear_orden(lista->ultimo->numero);
	}else{
		lista_neg->crear_orden(lista->ultimo->numero);
	}
	cout<<"Los numeros positivos son: "<<endl;
	lista_pos->imprimir();
	cout<<endl;
	cout<<"Los numeros negativos son: "<<endl;
	lista_neg->imprimir();
	
	
    return 0;
}
