#include <iostream>
#include "Nodo.h"
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    public:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

        /* constructor*/
        Lista();
        /* crea un nuevo nodo, recibe una instancia de la clase Persona. */
        void crear (int numero);
        void crear_orden(int numero);
        /* imprime la lista. */
        void imprimir ();
        /*crea y divide listas pos y neg*/
        void dividir(Lista lista_neg, Lista lista_pos);
};
#endif
