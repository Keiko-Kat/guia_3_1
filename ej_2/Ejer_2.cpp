#include "Nodo.h"
#include "Lista.h"
#include "Ejer_2.h"
#include <string>
#include <iostream>
#include <algorithm>
using namespace std;


int main (void) {
    Ejercicio e = Ejercicio();
    Lista *lista = e.get_lista();
    string in = "q";
    
    while(in != "s"){
		cout<<"Ingrese un nombre (o 's' para salir):"<<endl;
		cout<<"   ~~~> ";
		getline(cin, in);
		if(in != "s"){
			transform(in.begin(), in.end(),in.begin(), ::toupper);
			lista->crear_orden(in);    
			lista->imprimir();
		}
	}
	
	
	
    return 0;
}
