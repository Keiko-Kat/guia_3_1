#include <iostream>
#include <string>
using namespace std;

#include "Nodo.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear_orden (string nombre) {
    Nodo *tmp;
    Nodo *temp = this->raiz;
	Nodo *repl;

    /* crea un nodo . */
    tmp = new Nodo;
    /* asigna la instancia de Nombre. */
    tmp->nombre = nombre;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, se compara con el resto de los nombres de la lista. */
    }else {		
		while (temp != NULL) {
			/*si es menor al primero se inserta en su lugar*/
			if(tmp->nombre < temp->nombre){
				this->raiz = tmp;
				this->raiz->sig = temp;
				temp = NULL; break;
			}else{
				if(tmp->nombre >= temp->nombre){					
					if(temp->sig != NULL){
						repl = temp->sig;
						if(tmp->nombre <= temp->sig->nombre){
							temp->sig = tmp;
							tmp->sig = repl;
							temp = NULL; break;
						}
					}else{
						this->ultimo->sig = tmp;
						this->ultimo = tmp;
						temp = NULL; break;
					}
					
					temp = temp->sig;
				}
			}
		}
	}
}



void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;
	cout<<"Nombres: "<<endl;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    while (tmp != NULL) {
        cout <<"--"<< tmp->nombre <<" "<<endl;
        tmp = tmp->sig;
    }
    cout<< endl;
}
