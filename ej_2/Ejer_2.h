#include <string>
#include "Nodo.h"
#include "Lista.h"
using namespace std;



class Ejercicio {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Ejercicio() {
            this->lista = new Lista();
        }
        
        Lista *get_lista() {
            return this->lista;
        }
};
