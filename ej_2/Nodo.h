#include <string>
#include <iostream>
using namespace std;
#ifndef NODO_H
#define NODO_H

/* define la estructura del nodo. */
typedef struct _Nodo {
    string nombre;
    struct _Nodo *sig;
} Nodo;

#endif
