#include <iostream>
#include "Nodo.h"
#include <string>

using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    public:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

        /* constructor*/
        Lista();
        /* crea un nuevo nodo, recibe una instancia de Persona. */
        void crear_orden(string nombre);
        /* imprime la lista. */
        void imprimir ();
};
#endif
